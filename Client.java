package gestionAgenceLocationVoiture;

public class Client {
	
//Attributs
	
	private int code;
	private String nom;
	private String prenom;

		
//Constructeur

	public Client(int code, String nom, String prenom)
	{
	this.code=code;
	this.nom=nom;
	this.prenom=prenom;
		
	}

//Getteurs et setteurs

	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

// Méthode toString
	public String toString() 
	{
		return("Le client s'appelle : "+prenom+" "+nom+". Son code est : "+code);
	}
// Méthode pour comparer l'objet actuel avec un autre client
	public boolean equals(Client c)
	{
		return(this.code==c.getCode());
	}
	
	
	
}
