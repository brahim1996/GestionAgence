package gestionAgenceLocationVoiture;

public class Voiture {
	
//Attributs
	
	private int immatriculation;
	private int prix;
	private String marque;

//Constructeur
	
	Voiture(int immatriculation, int prix, String marque)
	{
		this.immatriculation=immatriculation;
		this.prix=prix;
		this.marque=marque;		
	}

//Getteurs et setteurs
		
	public int getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(int immatriculation) {
		this.immatriculation = immatriculation;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

//Déclaration de la méthode toString permettant de formater l'affichage d'un objet de type Voiture
	public String toString() {
		return("L'immatriculation de la voiture est : "+immatriculation+". Sa marque est :"+marque+". Son prix est : "+prix+" Euros.");
		
		
	}
//Méthode permettant de comparer l'objet actuel avec un autre objet
	
	public boolean equals(Voiture v) {
		if (v.getImmatriculation()==getImmatriculation())
			return true;
		return false;
		
		
		
	}
	


}
